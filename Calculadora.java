public abstract class Calculadora 
{
	
	public double suma(double n1, double n2)
	{
		double resul;
		resul = n1+n2;
		return resul;
	}
	public double resta(double n1, double n2)
	{
		double resul;
		resul = n1-n2;
		return resul;
	}
	public double multiplicacion(double n1, double n2)
	{
		double resul;
		resul = n1*n2;
		return resul;
	}
	public double division(double n1, double n2)
	{
		double resul;
		resul = n1/n2;
		return resul;
	}
}