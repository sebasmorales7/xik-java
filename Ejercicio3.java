import java.util.Scanner;
public class Ejercicio3 extends Calculadora 
{
	public static void main (String args[])
 	{
 		double numero1, numero2;
 		Scanner leer=new Scanner(System.in);
 		System.out.println("Ingrese Primer Numero: ");
 		numero1=leer.nextInt();
		System.out.println("Ingrese Segundo Numero: ");
 		numero2=leer.nextInt();
		Ejercicio3 c = new Ejercicio3();
		System.out.println("Calculadora:" + numero1 + "+" + numero2 + "=" + c.suma(numero1, numero2) );
		System.out.println("Calculadora:" + numero1 + "-" + numero2 + "=" + c.resta(numero1, numero2) );
		System.out.println("Calculadora:" + numero1 + "*" + numero2 + "=" + c.multiplicacion(numero1, numero2) );
		System.out.println("Calculadora:" + numero1 + "/" + numero2 + "=" + c.division(numero1, numero2) );
		
 	}

	
}