function Humano (name, age, colour) {
    this.name = name;
	this.age = age;
    this.colour = colour;
    
}
Humano.prototype.getName = function()
{
	console.log(this.name);
};
Humano.prototype.getAge = function()
{
	console.log(this.age);
};
Humano.prototype.getColor = function()
{
	console.log(this.colour);
};
Humano.prototype.toJSON = function() {
    console.log(this.name + ' ' + this.age + ' '  + this.colour);
};


var h = new Humano('miguel', 20, 'azul');
h.getName();
h.getAge();
h.getColor();
h.toJSON();