import java.util.*;
public class Ejercicio5 
{
	static Hashtable<Integer, Person> contenedor=new Hashtable<Integer,Person>();
	
	public static void main (String args[])
 	{	
		Scanner leer=new Scanner(System.in);
		int select =0;
		Integer dpi;
		int age;
		String firstname, lastname, job;
 		while(select != 5){
			System.out.println("--Menu--:\n1.- Register a Person" +"\n2.- Show the Names\n" +"3.- Show all the ages\n" +"4.- Show all the information of a specific person\n" +"5.- Exit");
			select = leer.nextInt(); 
				
			switch(select){
				case 1: 
				System.out.println("Enter your DPI: ");
				dpi= new Integer(leer.nextInt());
				leer.nextLine();
				System.out.println("Enter your First Name: ");
				firstname= leer.nextLine();
				System.out.println("Enter your Last Name: ");
				lastname= leer.nextLine();
				System.out.println("Enter your Age: ");
				age= leer.nextInt();
				leer.nextLine();
				System.out.println("Do you have a job (yes/no)?: ");
				job= leer.nextLine();
				Person personita = new Person(firstname,lastname,age,job);
				contenedor.put(dpi,personita);
				leer.nextLine();
				break;
				
				case 2: 
					
					Enumeration<Integer> e = contenedor.keys();
					Integer llave = 0;
					while(e.hasMoreElements()){
					llave  = e.nextElement();
					System.out.println(llave + ":" + " " + contenedor.get(llave).getName());
					}
					leer.nextLine();
					leer.nextLine();
					break;
				case 3: 
						Enumeration<Integer> t = contenedor.keys();
					Integer llave2 = 0;
					while(t.hasMoreElements())
					{
					llave2  = t.nextElement();
					System.out.println(llave2 + ":" + " " + contenedor.get(llave2).getAge());
					}
					leer.nextLine();
					leer.nextLine();
					break;
				case 4: 
							Enumeration<Integer> t1 = contenedor.keys();
					Integer llave3 = 0;
					while(t1.hasMoreElements()){
					llave3  = t1.nextElement();
					System.out.println(llave3 + ":" + " " + contenedor.get(llave3).getName()+ " " + contenedor.get(llave3).getLastName()+ " " + contenedor.get(llave3).getAge()+ " " + contenedor.get(llave3).getEmploymentSituation());
					}
					leer.nextLine();
					leer.nextLine();
					break;
				case 5: 
					System.out.println("Adios!");
					break;
			
			}
		}	
 	}
}