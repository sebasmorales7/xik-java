package functionalTests;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

import auxClasses.Browsers;
import auxClasses.ReadUrlFile;

public class Ejercicio3 extends Browsers {
	@Test
	public void f() {
		try {
			driver.get("http://104.131.93.237/file-storage");
			driver.findElement(By.id("email")).sendKeys("testx1@gmail.com");
			driver.findElement(By.id("password")).sendKeys("Ab1234");
			ReadUrlFile.Wait(2000);
			driver.findElement(By.id("bt_send")).click();
			ReadUrlFile.Wait(2000);
			driver.findElement(By.id("new_folder")).click();
			ReadUrlFile.Wait(2000);
			driver.findElement(By.id("new_folder_name")).sendKeys("moveFolder");
			ReadUrlFile.Wait(2000);
			driver.findElement(By.xpath("//button[contains(.,'New directory')]")).click();
			ReadUrlFile.Wait(2000);
			driver.findElement(By.xpath("//parent::div[@class='filename' and text()='moveFolder']/../div[3]")).click();
			ReadUrlFile.Wait(2000);
			driver.findElement(By.id("context_move")).click();
			ReadUrlFile.Wait(2000);
			driver.findElement(By.xpath("//div[@class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-draggable']/div[@class= 'modal-dialog ui-dialog-content ui-widget-content jstree jstree-1 jstree-focused jstree-classic']/ul[@class='jstree-no-dots']/li[@class='jstree-open jstree-last']/ul/li[@class='jstree-last jstree-leaf']/a[last()]")).click();
			ReadUrlFile.Wait(2000);
			driver.findElement(By.xpath("//div[7]/div[3]/div/button[contains(.,'Save')]")).click();
			ReadUrlFile.Wait(2000);
			driver.findElement(By.xpath("//parent::div[@class='filename']/../div[2]")).click();
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}
	

}
