package functionalTests;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

import auxClasses.Browsers;
import auxClasses.ReadUrlFile;

public class Ejercicio1 extends Browsers {
	@Test
	public void f() {
		try {
			driver.get("http://104.131.93.237/file-storage");
			driver.findElement(By.id("email")).sendKeys("testx1@gmail.com");
			driver.findElement(By.id("password")).sendKeys("Ab1234");
			ReadUrlFile.Wait(2000);
			driver.findElement(By.id("bt_send")).click();
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}
	

}
