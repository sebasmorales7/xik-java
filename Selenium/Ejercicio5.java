package functionalTests;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

import auxClasses.Browsers;
import auxClasses.ReadUrlFile;
import java.awt.Robot;
import java.awt.event.InputEvent;
//import java.awt.event.KeyEvent;

public class Ejercicio5 extends Browsers {
	@Test
	public void f() {
		try {
			driver.get("http://104.131.93.237/file-storage");
			driver.findElement(By.id("email")).sendKeys("testx1@gmail.com");
			driver.findElement(By.id("password")).sendKeys("Ab1234");
			ReadUrlFile.Wait(2000);
			driver.findElement(By.id("bt_send")).click();
			ReadUrlFile.Wait(2000);
			
			Robot robot = new Robot();
			robot.delay(250);
			driver.findElement(By.xpath("//parent::div[@class='filename' and text()='D2']")).click();
			ReadUrlFile.Wait(5000);
		    robot.mouseMove(685, 350);
		    ReadUrlFile.Wait(3000);
		    robot.mousePress(InputEvent.BUTTON1_MASK);
		    robot.mouseMove(585, 350);
		    robot.mouseRelease(InputEvent.BUTTON1_MASK);
		    ReadUrlFile.Wait(3000);
		    robot.mouseMove(585, 350);
		    robot.mousePress(InputEvent.BUTTON1_MASK);
		    ReadUrlFile.Wait(2000);
		    robot.mouseRelease(InputEvent.BUTTON1_MASK);
		    
		    
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
		
	}
	
}