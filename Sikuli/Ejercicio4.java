package visualTests;

import org.sikuli.api.robot.Key;
import org.sikuli.basics.Settings;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Region;
import org.sikuli.script.Screen;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import auxClasses.Browsers;
import auxClasses.ReadUrlFile;

/**
 * Test if SikuliX has been set up properly.
 * @author Pablo Miranda
 *
 */
// Browsers is a class that initiates the WebDriver depending on the parameter passed by the build.xml file. 
// Otherwise it has a default value that can me modified at auxClasses/Browsers at the @Optional tag
public class Ejercicio4 extends BCLASS{
	
	
	@Test
	public void f() {
		try {
			ReadUrlFile.Wait(3000);
			Region add = screen.wait("img/ejercicio4/addf.png");
			add.click();
			Region folder = screen.wait("img/ejercicio4/addFile.png");
			folder.click();
			ReadUrlFile.Wait(3000);
			Region t = screen.wait("img/ejercicio4/training.png");
			t.click();
			ReadUrlFile.Wait(3000);
			Region abrir = screen.wait("img/ejercicio4/openFile.png");
			abrir.click();
			ReadUrlFile.Wait(3000);
			Region start = screen.wait("img/ejercicio4/start.png");
			start.click();
			
			ReadUrlFile.Wait(3000);
			Region goback = screen.wait("img/ejercicio4/goback.png");
			goback.click();
			ReadUrlFile.Wait(3000);
			Region foler = screen.wait("img/ejercicio4/t.png");
			Region bull = foler.wait("img/ejercicio4/bullet.png");
			bull.click();
			ReadUrlFile.Wait(3000);
			Region borrar = screen.wait("img/ejercicio4/delete.png");
			borrar.click();
			ReadUrlFile.Wait(3000);
			Region borrarsi = screen.wait("img/ejercicio4/d.png");
			borrarsi.click();
			ReadUrlFile.Wait(5000);
			
			Region trash = screen.wait("img/ejercicio4/trashbin.png");
			trash.click();
			ReadUrlFile.Wait(3000);
			Region darlec = screen.wait("img/ejercicio4/t.png");
			bull = darlec.wait("img/ejercicio4/bullet.png");
			bull.click();
			borrar = screen.wait("img/ejercicio4/delete.png");
			borrar.click();
			borrarsi = screen.wait("img/ejercicio4/d.png");
			borrarsi.click();
			
			ReadUrlFile.Wait(3000);
			Region homedir = screen.wait("img/ejercicio4/homedir.png");
			homedir.click();
			
		} catch (FindFailed e) {
			System.out.println(e.getMessage());
			Assert.fail(e.getMessage());
		} catch (Exception e1) {
			Assert.fail(e1.getMessage());
		}
	}
}