package visualTests;

import org.sikuli.api.robot.Key;
import org.sikuli.basics.Settings;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Region;
import org.sikuli.script.Screen;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import auxClasses.Browsers;
import auxClasses.ReadUrlFile;

/**
 * Test if SikuliX has been set up properly.
 * @author Pablo Miranda
 *
 */
// Browsers is a class that initiates the WebDriver depending on the parameter passed by the build.xml file. 
// Otherwise it has a default value that can me modified at auxClasses/Browsers at the @Optional tag
public class Ejercicio2 extends BCLASS{
	
	
	@Test
	public void f() {
		try {
			ReadUrlFile.Wait(3000);
			Region add = screen.wait("img/ejercicio2/add.png");
			add.click();
			Region folder = screen.wait("img/ejercicio2/foldername.png");
			folder.click();
			folder.type("Training" );
			Region newd = screen.wait("img/ejercicio2/newdirectory.png");
			newd.click();
			ReadUrlFile.Wait(3000);
			Region darlec = screen.wait("img/ejercicio2/Training.png");
			darlec.click();
			ReadUrlFile.Wait(3000);
			Region homedir = screen.wait("img/ejercicio2/homedir.png");
			homedir.click();
			darlec = screen.wait("img/ejercicio2/Training.png");
			Region bull = darlec.wait("img/ejercicio2/bullet.png");
			bull.click();
			Region borrar = screen.wait("img/ejercicio2/delete.png");
			borrar.click();
			Region borrarsi = screen.wait("img/ejercicio2/suredelete.png");
			borrarsi.click();
			ReadUrlFile.Wait(5000);
			
			
			Region trash = screen.wait("img/ejercicio2/trashbin.png");
			trash.click();
			ReadUrlFile.Wait(3000);
			darlec = screen.wait("img/ejercicio2/Training.png");
			bull = darlec.wait("img/ejercicio2/bullet.png");
			bull.click();
			borrar = screen.wait("img/ejercicio2/delete.png");
			borrar.click();
			borrarsi = screen.wait("img/ejercicio2/suredelete.png");
			borrarsi.click();
			
			ReadUrlFile.Wait(3000);
			homedir = screen.wait("img/ejercicio2/homedir.png");
			homedir.click();
			/*Region pass = screen.wait("img/ejercicio1/pw.png");
			pass.click();
			pass.type("Ab1234");
			Region butonlog = screen.wait("img/ejercicio1/button.png");
			butonlog.click();*/
			
			//screen.wait("img/link.png").click();
			
		} catch (FindFailed e) {
			System.out.println(e.getMessage());
			Assert.fail(e.getMessage());
		} catch (Exception e1) {
			Assert.fail(e1.getMessage());
		}
	}
}	