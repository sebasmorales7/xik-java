package visualTests;

import org.sikuli.api.robot.Key;
import org.sikuli.basics.Settings;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Region;
import org.sikuli.script.Screen;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import auxClasses.Browsers;
import auxClasses.ReadUrlFile;

/**
 * Test if SikuliX has been set up properly.
 * @author Pablo Miranda
 *
 */
// Browsers is a class that initiates the WebDriver depending on the parameter passed by the build.xml file. 
// Otherwise it has a default value that can me modified at auxClasses/Browsers at the @Optional tag
public class Ejercicio5 extends BCLASS{
	
	
	@Test
	public void f() {
		try {
			ReadUrlFile.Wait(4000);
			Region add = screen.wait("img/ejercicio5/add.png");
			add.click();
			Region folder = screen.wait("img/ejercicio5/foldername.png");
			folder.click();
			folder.type("D1" );
			Region newd = screen.wait("img/ejercicio5/newdirectory.png");
			newd.click();
			ReadUrlFile.Wait(3000);
			add = screen.wait("img/ejercicio5/add.png");
			add.click();
			Region folder2 = screen.wait("img/ejercicio5/foldername.png");
			folder2.click();
			folder2.type("D2" );
			newd = screen.wait("img/ejercicio5/newdirectory.png");
			newd.click();
			ReadUrlFile.Wait(5000);
			//folder2.drag("img/ejercicio5/D1.png");
			//Region ajustar = screen.wait("img/ejercicio5/D2O.png");
			screen.dragDrop("img/ejercicio5/D2A.png", "img/ejercicio5/D2.png");
			ReadUrlFile.Wait(5000);
			Region flder1 = screen.wait("img/ejercicio5/D2.png");
			flder1.click();
			ReadUrlFile.Wait(3000);
			Region flder2 = screen.wait("img/ejercicio5/D2.png");
			Region bull = flder2.wait("img/ejercicio5/bullet.png");
			bull.click();
			ReadUrlFile.Wait(3000);
			Region borrar = screen.wait("img/ejercicio5/delete.png");
			borrar.click();
			ReadUrlFile.Wait(3000);
			Region borrarsi = screen.wait("img/ejercicio5/d.png");
			borrarsi.click();
			ReadUrlFile.Wait(3000);
			Region homedir = screen.wait("img/ejercicio5/homedir.png");
			homedir.click();
			ReadUrlFile.Wait(3000);
			flder1 = screen.wait("img/ejercicio5/D2.png");
			bull = flder1.wait("img/ejercicio5/bullet.png");
			bull.click();
			ReadUrlFile.Wait(3000);
			borrar = screen.wait("img/ejercicio5/delete.png");
			borrar.click();
			ReadUrlFile.Wait(3000);
			borrarsi = screen.wait("img/ejercicio5/d.png");
			borrarsi.click();
			
			ReadUrlFile.Wait(5000);
			Region trash = screen.wait("img/ejercicio5/trashbin.png");
			trash.click();
			ReadUrlFile.Wait(3000);
			Region darlec = screen.wait("img/ejercicio5/D2.png");
			bull = darlec.wait("img/ejercicio5/bullet.png");
			bull.click();
			borrar = screen.wait("img/ejercicio5/delete.png");
			borrar.click();
			borrarsi = screen.wait("img/ejercicio5/d.png");
			borrarsi.click();

			ReadUrlFile.Wait(5000);
			darlec = screen.wait("img/ejercicio5/D1.png");
			bull = darlec.wait("img/ejercicio5/bullet.png");
			bull.click();
			borrar = screen.wait("img/ejercicio5/delete.png");
			borrar.click();
			borrarsi = screen.wait("img/ejercicio5/d.png");
			borrarsi.click();

			
			ReadUrlFile.Wait(3000);
			homedir = screen.wait("img/ejercicio5/homedir.png");
			homedir.click();
			
			/*darlec = screen.wait("img/ejercicio2/Training.png");
			Region bull = darlec.wait("img/ejercicio2/bullet.png");
			bull.click();
			Region borrar = screen.wait("img/ejercicio2/delete.png");
			borrar.click();
			Region borrarsi = screen.wait("img/ejercicio2/suredelete.png");
			borrarsi.click();*/
			
		} catch (FindFailed e) {
			System.out.println(e.getMessage());
			Assert.fail(e.getMessage());
		} catch (Exception e1) {
			Assert.fail(e1.getMessage());
		}
	}
}