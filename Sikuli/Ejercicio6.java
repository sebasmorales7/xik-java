package visualTests;

import org.sikuli.api.robot.Key;
import org.sikuli.basics.Settings;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Region;
import org.sikuli.script.Screen;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import auxClasses.Browsers;
import auxClasses.ReadUrlFile;

/**
 * Test if SikuliX has been set up properly.
 * @author Pablo Miranda
 *
 */
// Browsers is a class that initiates the WebDriver depending on the parameter passed by the build.xml file. 
// Otherwise it has a default value that can me modified at auxClasses/Browsers at the @Optional tag
public class Ejercicio6 extends BCLASS{
	
	
	@Test
	public void f() {
		try {
			ReadUrlFile.Wait(5000);
			Region add = screen.wait("img/ejercicio6/add.png");
			add.click();
			Region folder = screen.wait("img/ejercicio6/foldername.png");
			folder.click();
			folder.type("COM1" );
			Region newd = screen.wait("img/ejercicio6/newdirectory.png");
			newd.click();
			ReadUrlFile.Wait(500);
			Region IN = screen.wait("img/ejercicio6/IN.png");
			if(screen.exists(IN) != null)
			{
				System.out.println("MENSAJE ENCONTRADO");
			}
				
			
		} catch (FindFailed e) {
			System.out.println(e.getMessage());
			Assert.fail(e.getMessage());
		} catch (Exception e1) {
			Assert.fail(e1.getMessage());
		}
	}
}	
