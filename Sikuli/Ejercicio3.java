package visualTests;

import org.sikuli.api.robot.Key;
import org.sikuli.basics.Settings;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Region;
import org.sikuli.script.Screen;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import auxClasses.Browsers;
import auxClasses.ReadUrlFile;

/**
 * Test if SikuliX has been set up properly.
 * @author Pablo Miranda
 *
 */
// Browsers is a class that initiates the WebDriver depending on the parameter passed by the build.xml file. 
// Otherwise it has a default value that can me modified at auxClasses/Browsers at the @Optional tag
public class Ejercicio3 extends BCLASS{
	
	
	@Test
	public void f() {
		try {
			ReadUrlFile.Wait(3000);
			Region add = screen.wait("img/ejercicio3/add.png");
			add.click();
			Region folder = screen.wait("img/ejercicio3/foldername.png");
			folder.click();
			folder.type("moveFolder" );
			Region newd = screen.wait("img/ejercicio3/newdirectory.png");
			newd.click();
			ReadUrlFile.Wait(3000);
			Region darlec = screen.wait("img/ejercicio3/folder.png");
			Region bull = darlec.wait("img/ejercicio3/bullet.png");
			bull.click();
			Region mover = screen.wait("img/ejercicio3/Move.png");
			mover.click();
			Region menu = screen.wait("img/ejercicio3/menu.png");
			Region trash = screen.wait("img/ejercicio3/trashbin.png");
			trash.click();
			Region save = screen.wait("img/ejercicio3/save.png");
			save.click();
			ReadUrlFile.Wait(5000);
			trash = screen.wait("img/ejercicio3/trashbin.png");
			trash.click();
			ReadUrlFile.Wait(3000);
			darlec = screen.wait("img/ejercicio3/folder.png");
			bull = darlec.wait("img/ejercicio3/bullet.png");
			bull.click();
			Region borrar = screen.wait("img/ejercicio3/delete.png");
			borrar.click();
			Region borrarsi = screen.wait("img/ejercicio3/suredelete.png");
			borrarsi.click();
			
			ReadUrlFile.Wait(3000);
			Region homedir = screen.wait("img/ejercicio3/homedir.png");
			homedir.click();
			
		} catch (FindFailed e) {
			System.out.println(e.getMessage());
			Assert.fail(e.getMessage());
		} catch (Exception e1) {
			Assert.fail(e1.getMessage());
		}
	}
}	