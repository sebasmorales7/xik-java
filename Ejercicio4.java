import java.util.Scanner;
public class Ejercicio4  
{
	public static void main (String args[])
	{
		double numero1;
 		Scanner leer=new Scanner(System.in);
 		System.out.println("Ingrese Primer Numero: ");
 		numero1=leer.nextDouble();
		Circle c = new Circle(numero1);
		Sphere s = new Sphere(numero1);
		
		System.out.println("Circle Characteristics");
		System.out.println("Side:" + " " + numero1 );
		System.out.println("Diameter:" +  " " +  c.calculateDiameter() );
		System.out.println("Circumference:" + " " + c.calculateCircumference() );
		System.out.println("Area:" + " " + c.calculateArea() );
		System.out.println("\n");
		System.out.println("Sphere Characteristics");
		System.out.println("Side:" + " " + numero1 );
		System.out.println("Diameter:" +  " " +  s.calculateDiameter() );
		System.out.println("Circumference:" + " " + s.calculateCircumference() );
		System.out.println("Area:" + " " + s.calculateAreaS() );
		

	}	
}